﻿using IMDb.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IMDb.Business.Interfaces
{
    public interface IMovieService
    {
        Task<bool> Add(Movie movie);
        Task<bool> Update(Movie movie);
    }
}
