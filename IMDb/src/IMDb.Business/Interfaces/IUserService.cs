﻿using IMDb.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IMDb.Business.Interfaces
{
    public interface IUserService
    {
        Task<bool> Add(User user);
        Task<bool> Update(User user);
    }
}
