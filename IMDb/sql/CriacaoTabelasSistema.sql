﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Movies] (
    [Id] uniqueidentifier NOT NULL,
    [Title] varchar(200) NOT NULL,
    [Genre] varchar(150) NOT NULL,
    [Director] varchar(250) NOT NULL,
    [Actors] varchar(1000) NOT NULL,
    [Year] int NOT NULL,
    CONSTRAINT [PK_Movies] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Users] (
    [Id] uniqueidentifier NOT NULL,
    [UserName] varchar(200) NOT NULL,
    [Password] varchar(200) NOT NULL,
    [Role] int NOT NULL,
    [Active] bit NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Ratings] (
    [UserId] uniqueidentifier NOT NULL,
    [MovieId] uniqueidentifier NOT NULL,
    [Rate] int NOT NULL,
    CONSTRAINT [PK_Ratings] PRIMARY KEY ([UserId], [MovieId]),
    CONSTRAINT [FK_Ratings_Movies_MovieId] FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Ratings_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);
GO

CREATE INDEX [IX_Ratings_MovieId] ON [Ratings] ([MovieId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210319023036_Initial', N'5.0.4');
GO

COMMIT;
GO


insert into users ([Id], [UserName], [Password], [Role], [Active]) values (NEWID(), 'Fernando', '123456', 1, 1)
insert into users ([Id], [UserName], [Password], [Role], [Active]) values (NEWID(), 'Roberto', '123456', 1, 0)




insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'Avatar', 'Action|Adventure|Fantasy|Sci-Fi', 'CCH Pounder', 'James Cameron', 2009)
insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'Pirates of the Caribbean: At World''s End', 'Action|Adventure|Fantasy', 'Johnny Depp', 'Gore Verbinski', 2007)
insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'Spectre', 'Action|Adventure|Thriller', 'Christoph Waltz', 'Sam Mendes', 2015)
insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'The Dark Knight Rises', 'Action|Thriller', 'Tom Hardy', 'Christopher Nolan', 2012)
insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'Star Wars: Episode VII - The Force Awakens', 'Documentary', 'Doug Walker', 'Doug Walker', 2010)
insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'John Carter', 'Action|Adventure|Sci-Fi', 'Daryl Sabara', 'Andrew Stanton', 2012)
insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'Spider-Man 3', 'Action|Adventure|Romance', 'J.K. Simmons', 'Sam Raimi', 2007)
insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'Tangled ', 'Adventure|Animation|Comedy|Family|Fantasy|Musical|Romance', 'Brad Garrett', 'Nathan Greno', 2010)
insert into Movies ([Id], [Title], [Genre], [Director], [Actors], [Year]) values (NEWID(), 'Avengers: Age of Ultron', 'Action|Adventure|Sci-Fi', 'Chris Hemsworth', 'Joss Whedon', 2015)

