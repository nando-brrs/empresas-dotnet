﻿using IMDb.Business.Interfaces;
using IMDb.Business.Models;
using IMDb.Business.Validations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IMDb.Business.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository,
            INotificador notificador) : base(notificador)
        {
            _userRepository = userRepository;
        }
        public async Task<bool> Add(User user)
        {
            if (!ExecutarValidacao(new UserValidation(), user)) return false;

            await _userRepository.Add(user);

            return true;
        }

        public async Task<bool> Update(User user)
        {
            if (!ExecutarValidacao(new UserValidation(), user)) return false;

            await _userRepository.Add(user);

            return true;
        }
    }
}
