﻿using AutoMapper;
using IMDb.Api.Configuration;
using IMDb.Api.ViewModel;
using IMDb.Business.Interfaces;
using IMDb.Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDb.Api.Controllers
{
    [Route("api/movies")]
    public class MoviesController : MainController
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMovieService _movieService;
        private readonly IRatingRepository _ratingRepository;
        private readonly IMapper _mapper;
        public MoviesController(IMovieRepository movieRepository,
                                IRatingRepository ratingRepository,
                                IMovieService movieService,
                                IMapper mapper,
                                INotificador notificador) : base(notificador)
        {
            _ratingRepository = ratingRepository;
            _movieRepository = movieRepository;
            _movieService = movieService;
            _mapper = mapper;
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<MovieViewModel>> GetAll()
        {

            var movies = _mapper.Map<IEnumerable<MovieViewModel>>(await _movieRepository.GetAll());

            return movies;
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("GetPaged")]
        public JsonResult GetPaged([FromQuery] PagedParamViewModel pagedParam, [FromBody] MoviesFilterViewModel moviesFilter)
        {
            List<Expression<Func<Movie, bool>>> where = new List<Expression<Func<Movie, bool>>>();

            Expression<Func<Movie, object>> orderBy = null;

            if (!string.IsNullOrEmpty(moviesFilter.Title))
                where.Add(m => m.Title.Contains(moviesFilter.Title));

            if (!string.IsNullOrEmpty(moviesFilter.Actors))
                 where.Add(m => m.Actors == moviesFilter.Actors);

            if (!string.IsNullOrEmpty(moviesFilter.Director))
                where.Add(m => m.Director.Contains(moviesFilter.Director));

            if (!string.IsNullOrEmpty(moviesFilter.Genre))
                where.Add(m => m.Genre.Contains(moviesFilter.Genre));

            if (pagedParam.orderBy == "Title")
                orderBy = c => c.Title;

            var result = _movieRepository.GetPaged(pagedParam.pageIndex, pagedParam.pageSize, where, orderBy, pagedParam.ascending);

            var movies = _mapper.Map<IEnumerable<MovieViewModel>>(result.Item2);

            return Json(new
            {
                Result = movies,
                Total = result.Item1
            });
        }
        [HttpGet("{id:guid}")]
        [AllowAnonymous]
        public async Task<ActionResult<MovieViewModel>> GetById(Guid id)
        {
            var movie = _mapper.Map<MovieViewModel>(await _movieRepository.GetById(id));

            if (movie == null) return NotFound();

            return movie;
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<MovieViewModel>> Add([FromBody] MovieViewModel movieViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var movie = _mapper.Map<Movie>(movieViewModel);
            await _movieService.Add(movie);

            return CustomResponse(movieViewModel);



        }
        [HttpPut("{id:guid}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<MovieViewModel>> Update(Guid id, [FromBody] MovieViewModel movieViewModel)
        {
            if (id != movieViewModel.Id) return BadRequest();

            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var movie = _mapper.Map<Movie>(movieViewModel);
            await _movieService.Update(movie);

            return CustomResponse(movieViewModel);



        }
        [HttpPost]
        [Route("RateMovie")]
        [Authorize(Roles = "User")]
        public async Task<ActionResult<RatingViewModel>> RateMovie([FromBody] RatingViewModel ratingViewModel)
        {
            IdentityManager identityManager = new IdentityManager(User);

            if (identityManager.UserID == null)
                return NotFound();

            ratingViewModel.UserId = Guid.Parse(identityManager.UserID);

            var rating = (await _ratingRepository.Search(x => x.UserId == ratingViewModel.UserId &&
                                                     x.MovieId == ratingViewModel.MovieId)).FirstOrDefault();

            if (rating != null)
                return BadRequest(new { message = "Usuário já avaliou o filme" });

            if (!ModelState.IsValid) return BadRequest();

            var rate = _mapper.Map<Rating>(ratingViewModel);
            await _ratingRepository.Add(rate);

            return Ok(ratingViewModel);

        }
    }
}
