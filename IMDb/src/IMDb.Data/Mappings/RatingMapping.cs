﻿using IMDb.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Data.Mappings
{
    public class RatingMapping : IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            builder.HasKey(r => new { r.UserId, r.MovieId });

            builder.HasOne(r => r.User)
                .WithMany(u => u.Ratings)
                .HasForeignKey(r => r.UserId);

            builder.HasOne(r => r.Movie)
                .WithMany(u => u.Ratings)
                .HasForeignKey(r => r.MovieId);

            builder.Property(p => p.Rate)
                .IsRequired();

            builder.ToTable("Ratings");

        }
    }
}
