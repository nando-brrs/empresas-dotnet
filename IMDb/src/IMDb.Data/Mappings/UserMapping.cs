﻿using IMDb.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Data.Mappings
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.UserName)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.Password)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.Role)
                .IsRequired();

            builder.Property(p => p.Active)
                .IsRequired();

            builder.ToTable("Users");

        }
    }
}
