﻿using FluentValidation;
using IMDb.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Business.Validations
{
    public class MovieValidation : AbstractValidator<Movie>
    {
        public MovieValidation()
        {
            RuleFor(l => l.Title)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado")
                .Length(10, 200)
                .WithMessage("O Campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");

            RuleFor(l => l.Genre)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado")
                .Length(10, 150)
                .WithMessage("O Campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");

            RuleFor(l => l.Director)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado")
                .Length(10, 250)
                .WithMessage("O Campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");

            RuleFor(l => l.Actors)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado")
                .Length(10, 1000)
                .WithMessage("O Campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");

            RuleFor(l => l.Year)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado");
        }
    }
}
