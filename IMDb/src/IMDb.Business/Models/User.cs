﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IMDb.Business.Models
{
    public class User : Entity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
        public bool Active { get; set; }
        [JsonIgnore]
        virtual public ICollection<Rating> Ratings { get; set; }
    }
}
