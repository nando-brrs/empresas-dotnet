﻿using IMDb.Business.Interfaces;
using IMDb.Business.Models;
using IMDb.Business.Validations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IMDb.Business.Services
{
    public class MovieService : BaseService, IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        public MovieService(IMovieRepository movieRepository,
            INotificador notificador) : base(notificador)
        {
            _movieRepository = movieRepository;
        }
        public async Task<bool> Add(Movie movie)
        {
            if (!ExecutarValidacao(new MovieValidation(), movie)) return false;

            await _movieRepository.Add(movie);

            return true;
        }

        public async Task<bool> Update(Movie movie)
        {
            if (!ExecutarValidacao(new MovieValidation(), movie)) return false;

            await _movieRepository.Add(movie);

            return true;
        }
    }
}
