﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Api.ViewModel
{
    public class PagedParamViewModel
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public string orderBy { get; set; }
        public bool ascending { get; set; }
    }
}
