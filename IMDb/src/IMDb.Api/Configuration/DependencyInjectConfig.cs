﻿using IMDb.Business.Interfaces;
using IMDb.Business.Notifications;
using IMDb.Business.Services;
using IMDb.Data.Context;
using IMDb.Data.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Api.Configuration
{
    public static class DependencyInjectConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<IMDbContext>();

            services.AddScoped<INotificador, Notificador>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IMovieService, MovieService>();

            services.AddScoped<IRatingRepository, RatingRepository>();

            return services;

        }
    }
}
