﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Api.ViewModel
{
    public enum RoleViewModel
    {
        User,
        Admin
    }
}
