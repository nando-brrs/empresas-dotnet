﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMDb.Business.Models
{
    public class Movie : Entity
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Actors { get; set; }
        public int Year { get; set; }
        public decimal AverageRating {
            get
            {
                return GetAverageRating();
            }
        }
        virtual public ICollection<Rating> Ratings { get; set; }

        private decimal GetAverageRating()
        {
            decimal rating = 0;
            if (Ratings != null)
            {
                int ratingNum = Ratings.Count;


                if (ratingNum > 0)
                {
                    foreach (var item in Ratings)
                        rating = rating + item.Rate;

                    rating = Math.Ceiling((decimal)(rating / ratingNum));
                }
            }

            return rating;
        }
    }
}
