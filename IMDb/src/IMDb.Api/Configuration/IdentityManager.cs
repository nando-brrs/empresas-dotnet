﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IMDb.Api.Configuration
{
    public class IdentityManager
    {
        public ClaimsPrincipal User { get; set; }
        public IdentityManager(ClaimsPrincipal user)
        {
            User = user;
        }
        public bool IsAuthenticated { get { return User.Identity.IsAuthenticated; } }
        public string UserName { get { return User.FindFirst(ClaimTypes.Name).Value; } }
        public string UserID
        {
            get
            {
                var val = User.FindFirst("UserId");
                return val != null ? val.Value : "";
            }
        }
        public string GetValueByKey(string key)
        {
            var val = User.FindFirst(key);
            return val != null ? val.Value : "";
        }
    }
}
