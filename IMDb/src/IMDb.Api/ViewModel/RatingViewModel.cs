﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Api.ViewModel
{
    public class RatingViewModel
    {
        [Required(ErrorMessage = "O Campo {0} é obrigatorio")]
        public Guid UserId { get; set; }
        [Required(ErrorMessage = "O Campo {0} é obrigatorio")]
        public Guid MovieId { get; set; }
        [Required(ErrorMessage = "O Campo {0} é obrigatorio")]
        public int Rate { get; set; }
    }
}
