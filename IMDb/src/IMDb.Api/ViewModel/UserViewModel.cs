﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Api.ViewModel
{
    public class UserViewModel
    {
        [Key]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "O Campo {0} é obrigatorio")]
        [StringLength(200, ErrorMessage = "O Campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 5)]
        public string UserName { get; set; }
        [Required(ErrorMessage = "O Campo {0} é obrigatorio")]
        [StringLength(200, ErrorMessage = "O Campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 5)]
        public string Password { get; set; }
        [Required(ErrorMessage = "O Campo {0} é obrigatorio")]
        public RoleViewModel Role { get; set; }

        [Required(ErrorMessage = "O Campo {0} é obrigatorio")]
        public bool Active { get; set; }
    }
}
