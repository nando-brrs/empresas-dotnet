﻿using IMDb.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Data.Mappings
{
    public class MovieMapping : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Title)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.Genre)
                .IsRequired()
                .HasColumnType("varchar(150)");

            builder.Property(p => p.Director)
                .IsRequired()
                .HasColumnType("varchar(250)");

            builder.Property(p => p.Actors)
                .IsRequired()
                .HasColumnType("varchar(1000)");

            builder.Property(p => p.Year)
                .IsRequired();

            builder.ToTable("Movies");

        }
    }
}
