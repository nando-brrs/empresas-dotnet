﻿using AutoMapper;
using IMDb.Api.ViewModel;
using IMDb.Business.Interfaces;
using IMDb.Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDb.Api.Controllers
{
    [Route("api/users")]
    public class UsersController : MainController
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public UsersController(IUserRepository userRepository,
                                IUserService userService,
                                IMapper mapper,
                                INotificador notificador) : base(notificador)
        {
            _userRepository = userRepository;
            _userService = userService;
            _mapper = mapper;
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<UserViewModel>> GetAll()
        {

            var users = _mapper.Map<IEnumerable<UserViewModel>>(await _userRepository.GetAll());

            return users;
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("GetPaged")]
        public JsonResult GetPaged([FromQuery] PagedParamViewModel pagedParam, [FromBody] UsersFilterViewModel usersFilter)
        {
            List<Expression<Func<User, bool>>> where = new List<Expression<Func<User, bool>>>();

            Expression<Func<User, object>> orderBy = null;

            if (!string.IsNullOrEmpty(usersFilter.UserName))
                where.Add(m => m.UserName.Contains(usersFilter.UserName));

            if (usersFilter.Active != null)
                where.Add(m => m.Active == usersFilter.Active);

            if (usersFilter.Role != null)
                where.Add(m => m.Role == _mapper.Map<Role>(usersFilter.Role));

            if (pagedParam.orderBy == "UserName")
                orderBy = c => c.UserName;

            var result = _userRepository.GetPaged(pagedParam.pageIndex, pagedParam.pageSize, where, orderBy, pagedParam.ascending);

            var users = _mapper.Map<IEnumerable<UserViewModel>>(result.Item2);

            return Json(new
            {
                Result = users,
                Total = result.Item1
            });
        }
        [HttpGet("{id:guid}")]
        [AllowAnonymous]
        public async Task<ActionResult<UserViewModel>> GetById(Guid id)
        {
            var user = _mapper.Map<UserViewModel>(await _userRepository.GetById(id));

            if (user == null) return NotFound();

            return user;
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<UserViewModel>> Add([FromBody] UserViewModel UserViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var user = _mapper.Map<User>(UserViewModel);
            await _userService.Add(user);

            return CustomResponse(UserViewModel);


        }
        [HttpPut("{id:guid}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<UserViewModel>> Update( Guid id, [FromBody] UserViewModel UserViewModel)
        {

            if (id != UserViewModel.Id) return BadRequest();

            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var user = _mapper.Map<User>(UserViewModel);
            await _userService.Update(user);

            return CustomResponse(UserViewModel);

        }
        [HttpDelete("{id:guid}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<UserViewModel>> Delete(Guid id)
        {
            var user = await _userRepository.GetById(id);

            if (user == null) return NotFound();

            user.Active = false;

            await _userRepository.Update(user);

            return Ok(user);
        }
    }
}
