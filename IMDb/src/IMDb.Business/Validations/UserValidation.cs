﻿using FluentValidation;
using IMDb.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Business.Validations
{
    public class UserValidation : AbstractValidator<User>
    {
        public UserValidation()
        {
            RuleFor(l => l.UserName)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado")
                .Length(5, 200)
                .WithMessage("O Campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");

            RuleFor(l => l.Password)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado")
                .Length(5, 200)
                .WithMessage("O Campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");

            RuleFor(l => l.Role)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado");

            RuleFor(l => l.Active)
                .NotEmpty().WithMessage("O Campo {PropertyName} precisa ser informado");

        }
    }
}
