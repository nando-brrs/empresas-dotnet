﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Business.Models
{
    public class Rating
    {
        public Guid UserId { get; set; }
        virtual public User User { get; set; }
        public Guid MovieId { get; set; }
        virtual public Movie Movie { get; set; }
        public int Rate { get; set; }
    }
}
