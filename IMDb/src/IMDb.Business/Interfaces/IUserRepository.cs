﻿using IMDb.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Business.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
