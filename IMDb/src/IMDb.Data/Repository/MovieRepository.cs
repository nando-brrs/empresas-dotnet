﻿using IMDb.Business.Interfaces;
using IMDb.Business.Models;
using IMDb.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Data.Repository
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(IMDbContext context) : base(context) { }
    }
}
