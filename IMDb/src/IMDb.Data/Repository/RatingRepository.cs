﻿using IMDb.Business.Interfaces;
using IMDb.Business.Models;
using IMDb.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMDb.Data.Repository
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(IMDbContext context) : base(context) { }
    }
}
