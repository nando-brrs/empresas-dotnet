﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Api.ViewModel
{
    public class UsersFilterViewModel
    {
        public string UserName { get; set; }
        public bool? Active { get; set; }
        public RoleViewModel? Role { get; set; }
    }
}
